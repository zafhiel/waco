import axios from "axios";
import { config } from "../../Constants";

export const api = {
  authenticate,
  signup,
  getFavorites,
  rickMortyCharacters,
  rickMortyCharacterDetails,
  toggledFavorite,
  rickMortyCharactersQuery,
  getUserInto,
  updateUserInfo,
};

function authenticate(email, password) {
  return instance.post(
    "api/v1/auth/login",
    { email: email, password },
    {
      headers: { "Content-type": "application/json" },
    },
  );
}

function signup(user) {
  return instance.post("/api/v1/user/signup", user, {
    headers: { "Content-type": "application/json" },
  });
}

function getUserInto(user) {
  return instance.get("/api/v1/user/" + user.id, {
    headers: { Authorization: jwtAuth(user) },
  });
}

function updateUserInfo(user, userInfo) {
  return instance.patch("/api/v1/user/" + user.id, userInfo, {
    headers: { Authorization: jwtAuth(user) },
  });
}

function rickMortyCharacters(page) {
  return instance.get("https://rickandmortyapi.com/api/character?page=" + page);
}

function rickMortyCharactersQuery(ids) {
  return instance.get(
    "https://rickandmortyapi.com/api/character/[" + ids + "]",
  );
}

function rickMortyCharacterDetails(id) {
  return instance.get("https://rickandmortyapi.com/api/character/" + id);
}

function getFavorites(user) {
  return instance.get("/api/v1/favorite/", {
    headers: { Authorization: jwtAuth(user) },
  });
}
function toggledFavorite(user, characterID) {
  return instance.post(
    "/api/v1/favorite/",
    { user_id: user.id, character_id: characterID },
    {
      headers: {
        "Content-type": "application/json",
        Authorization: jwtAuth(user),
      },
    },
  );
}

// -- Axios

const instance = axios.create({
  baseURL: config.url.API_BASE_URL,
});

// -- Helper functions

function jwtAuth(user) {
  return `Bearer ${user.token}`;
}
