import React from "react";
import { Link } from "react-router-dom";
import { Container, Menu } from "semantic-ui-react";
import { useAuth } from "../context/AuthContext";

function Navbar() {
  const { getUser, userIsAuthenticated, userLogout } = useAuth();

  const logout = () => {
    userLogout();
  };

  const publicMenuStyle = () => {
    return userIsAuthenticated() ? { display: "none" } : { display: "block" };
  };

  const privatePageStyle = () => {
    return userIsAuthenticated() ? { display: "block" } : { display: "none" };
  };

  const getName = () => {
    const user = getUser();
    return user ? user.name : "";
  };

  return (
    <Menu
      inverted
      color="blue"
      stackable
      size="massive"
      style={{ borderRadius: 0 }}
    >
      <Container>
        <Menu.Item header>WacoServices - Test</Menu.Item>
        <Menu.Item as={Link} exact="true" to="/">
          Home
        </Menu.Item>
        <Menu.Item as={Link} to="/favorites" style={privatePageStyle()}>
          List Favorites
        </Menu.Item>
        <Menu.Item as={Link} to="/userpage" style={privatePageStyle()}>
          User Profile
        </Menu.Item>
        <Menu.Menu position="right">
          <Menu.Item as={Link} to="/login" style={publicMenuStyle()}>
            Login
          </Menu.Item>
          <Menu.Item as={Link} to="/signup" style={publicMenuStyle()}>
            Sign Up
          </Menu.Item>
          <Menu.Item
            header
            style={privatePageStyle()}
          >{`Hi ${getName()}`}</Menu.Item>
          <Menu.Item
            as={Link}
            to="/"
            style={privatePageStyle()}
            onClick={logout}
          >
            Logout
          </Menu.Item>
        </Menu.Menu>
      </Container>
    </Menu>
  );
}

export default Navbar;
