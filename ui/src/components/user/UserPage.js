import React, { useEffect, useState } from "react";
import { Button, Form, Grid, Segment, Message } from "semantic-ui-react";
import { useAuth } from "../context/AuthContext";
import { api } from "../misc/Api";
import { handleLogError } from "../misc/Helpers";

function UserPage() {
  const Auth = useAuth();
  const user = Auth.getUser();

  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [birthday, setBirthday] = useState("");
  const [email, setEmail] = useState("");
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  useEffect(() => {
    handleGetUserInfo();
  }, []);

  const handleGetUserInfo = async () => {
    try {
      const response = await api.getUserInto(user);

      setName(response.data.data.name);
      setAddress(response.data.data.address);
      if (response.data.data.birthday !== "0001-01-01T00:00:00Z") {
        setBirthday(response.data.data.birthday);
      }
      setEmail(response.data.data.email);
    } catch (error) {
      handleLogError(error);
    } finally {
    }
  };

  const handleInputChange = (e, { name, value }) => {
    if (name === "address") {
      setAddress(value);
    } else if (name === "name") {
      setName(value);
    } else if (name === "email") {
      setEmail(value);
    } else if (name === "birthday") {
      setBirthday(value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const userInfo = { address, birthday, email, name };

    try {
      const response = await api.updateUserInfo(user, userInfo);
      setName(response.data.data.name);
      setAddress(response.data.data.address);
      if (response.data.data.birthday !== "0001-01-01T00:00:00Z") {
        setBirthday(response.data.data.birthday);
      }
      setEmail(response.data.data.email);
    } catch (error) {
      handleLogError(error);
      if (error.response && error.response.data) {
        const errorData = error.response.data;
        let errorMessage = "Invalid fields";
        if (errorData.status === 409) {
          errorMessage = errorData.message;
        } else if (errorData.status === 400) {
          errorMessage = errorData.errors[0].defaultMessage;
        }
        setIsError(true);
        setErrorMessage(errorMessage);
      }
    }
  };

  return (
    <Grid textAlign="center">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Form size="large" onSubmit={handleSubmit}>
          <Segment>
            <Form.Input
              fluid
              name="name"
              icon="address card"
              iconPosition="left"
              placeholder="Name"
              value={name}
              onChange={handleInputChange}
            />
            <Form.Input
              fluid
              name="email"
              icon="at"
              iconPosition="left"
              placeholder="Email"
              value={email}
              onChange={handleInputChange}
            />
            <Form.Input
              fluid
              name="address"
              icon="building"
              iconPosition="left"
              placeholder="Address"
              value={address}
              onChange={handleInputChange}
            />
            <Form.Input
              fluid
              name="birthday"
              icon="birthday cake"
              iconPosition="left"
              placeholder="Birthday"
              value={birthday}
              onChange={handleInputChange}
            />
            <Button color="blue" fluid size="large">
              Update
            </Button>
          </Segment>
        </Form>
        {isError && <Message negative>{errorMessage}</Message>}
      </Grid.Column>
    </Grid>
  );
}

export default UserPage;
