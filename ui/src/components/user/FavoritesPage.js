import React, { useEffect, useState } from "react";
import {
  Container, Icon,
} from "semantic-ui-react";


import { api } from '../misc/Api'
import { handleLogError } from '../misc/Helpers'

import { useAuth } from "../context/AuthContext";

function FavoritesPage() {
    const Auth = useAuth();
    const user = Auth.getUser();
    const [characters, setCharacters] = useState([]);

    useEffect(() => {
      const fetchData = async () => {
        try {
          const responseFavorites = await api.getFavorites(user);
          const favoritesList = responseFavorites.data.data.map(favorite => favorite.character_id)

          const responseCharacters = await api.rickMortyCharactersQuery(favoritesList);
          setCharacters(responseCharacters.data);

        } catch (error) {
          handleLogError(error);
        }
      };

      fetchData();
    }, []);

    if (characters.length === 0) {
      return <h1>No data</h1>;
    }

    return (
        <Container text>

          <ul>
            {characters.map((character) => (
                    <li key={character.id}>
                      <Icon
                          name="like"
                          color="red"
                      />
                      {character.name}
                    </li>
                )
            )
            }
          </ul>
        </Container>
    );
}


export default FavoritesPage