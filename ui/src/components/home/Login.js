import React, { useState } from "react";
import { NavLink, Navigate } from "react-router-dom";
import { Button, Form, Grid, Segment, Message } from "semantic-ui-react";
import { useAuth } from "../context/AuthContext";
import { api } from "../misc/Api";
import { handleLogError } from "../misc/Helpers";

function Login() {
  const Auth = useAuth();
  const isLoggedIn = Auth.userIsAuthenticated();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isError, setIsError] = useState(false);

  const handleInputChange = (e, { name, value }) => {
    if (name === "email") {
      setEmail(value);
    } else if (name === "password") {
      setPassword(value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!(email && password)) {
      setIsError(true);
      return;
    }

    try {
      const response = await api.authenticate(email, password);
      const { id, token, name } = response.data;
      const authenticatedUser = { id, token, name };

      Auth.userLogin(authenticatedUser);

      setEmail("");
      setPassword("");
      setIsError(false);
    } catch (error) {
      handleLogError(error);
      setIsError(true);
    }
  };

  if (isLoggedIn) {
    return <Navigate to={"/"} />;
  }

  return (
    <Grid textAlign="center">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Form size="large" onSubmit={handleSubmit}>
          <Segment>
            <Form.Input
              fluid
              name="email"
              icon="at"
              iconPosition="left"
              placeholder="Email"
              value={email}
              onChange={handleInputChange}
            />
            <Form.Input
              fluid
              name="password"
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              value={password}
              onChange={handleInputChange}
            />
            <Button color="blue" fluid size="large">
              Login
            </Button>
          </Segment>
        </Form>
        <Message>
          {`Don't have already an account? `}
          <NavLink to="/signup" as={NavLink} color="teal">
            Sign Up
          </NavLink>
        </Message>
        {isError && (
          <Message negative>
            The email or password provided are incorrect!
          </Message>
        )}
      </Grid.Column>
    </Grid>
  );
}

export default Login;
