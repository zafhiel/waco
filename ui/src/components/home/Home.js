import React, { useState, useEffect } from "react";
import {
  Container,
  Segment,
  Dimmer,
  Loader,
  Pagination,
  ModalHeader,
  ModalDescription,
  ModalContent,
  ModalActions,
  Button,
  Header,
  Image,
  Modal,
  Icon,
} from "semantic-ui-react";
import { api } from "../misc/Api";
import { handleLogError } from "../misc/Helpers";
import { useAuth } from "../context/AuthContext";

function Home() {
  const Auth = useAuth();
  const user = Auth.getUser();

  const [characters, setCharacters] = useState([]);
  const [characterDetails, setCharacterDetail] = useState([]);
  const [favorites, setFavorites] = useState([]);
  const [totalPages, setTotalPages] = useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const responseCharacters = await api.rickMortyCharacters("1");
        setCharacters(responseCharacters.data.results);
        setTotalPages(responseCharacters.data.info.pages);
        if (Auth.userIsAuthenticated()) {
          const responseFavorites = await api.getFavorites(user);
          const favoritesList = responseFavorites.data.data.map(favorite => favorite.character_id)
          setFavorites(favoritesList);
        }
      } catch (error) {
        handleLogError(error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  async function pageChangeg(_, pageData) {
    setIsLoading(true);
    try {
      const responseCharacters = await api.rickMortyCharacters(
          pageData.activePage,
      );
      setCharacters(responseCharacters.data.results);
    } catch (error) {
      handleLogError(error);
    } finally {
      setIsLoading(false);
    }
  }

  async function charaterDetails(id) {
    setIsLoading(true);
    try {
      const responseCharacterDetails =
          await api.rickMortyCharacterDetails(id);
      setCharacterDetail(responseCharacterDetails.data);
      setOpen(true);
    } catch (error) {
      handleLogError(error);
    } finally {
      setIsLoading(false);
    }
  }

  async function toggledFavorite(characterID) {
    if (user) {
      setIsLoading(true);
      try {
        await api.toggledFavorite(
            user,
            characterID,
        );

        const responseFavorites = await api.getFavorites(user);
        const favoritesList = responseFavorites.data.data.map(favorite => favorite.character_id)
        setFavorites(favoritesList);

      } catch (error) {
        handleLogError(error);
      } finally {
        setIsLoading(false);
      }
    }
  }

  if (isLoading) {
    return (
        <Segment basic style={{ marginTop: window.innerHeight / 2 }}>
          <Dimmer active inverted>
            <Loader inverted size="huge">
              Loading
            </Loader>
          </Dimmer>
        </Segment>
    );
  }

  return (
      <Container text>


        <ul>
          {characters.map((character) => (
                  <li key={character.id}>
                    <Icon
                        name="like"
                        color={favorites.includes(character.id) ? "red" : "grey"}
                        onClick={() => toggledFavorite(character.id)}
                    />
                    <a onClick={() => charaterDetails(character.id)} >{character.name}</a>
                  </li>
              )
          )
          }
        </ul>
{/*<ItemGroup>*/}
        {/*  {characters.map((character) => (*/}
        {/*      <Item key={character.id}>*/}
        {/*        <ItemImage*/}
        {/*            size="tiny"*/}
        {/*            src={character.image}*/}
        {/*            onClick={() => charaterDetails(character.id)}*/}
        {/*        />*/}

        {/*        <ItemContent verticalAlign="middle">*/}
        {/*          <ItemHeader>*/}
        {/*            <Icon*/}
        {/*                name="like"*/}
        {/*                color={favorites.includes(character.id) ? "red" : "grey"}*/}
        {/*                onClick={() => toggledFavorite(character.id)}*/}
        {/*            />*/}
        {/*            {character.name}*/}
        {/*          </ItemHeader>*/}
        {/*        </ItemContent>*/}
        {/*      </Item>*/}
        {/*  ))}*/}
        {/*</ItemGroup>*/}


        <Pagination
            defaultActivePage={1}
            totalPages={totalPages}
            onPageChange={pageChangeg}
        />
        <Modal
            onClose={() => setOpen(false)}
            onOpen={() => setOpen(true)}
            open={open}
            // trigger={<Button>Show Modal</Button>}
        >
          <ModalHeader>Details</ModalHeader>
          <ModalContent image>
            <Image size="medium" src={characterDetails.image} wrapped />
            <ModalDescription>
              <Header>{characterDetails.name} </Header>
              <ul>
                <li>
                  <strong>Status:</strong> {characterDetails.status}
                </li>
                <li>
                  <strong>Species:</strong> {characterDetails.species}
                </li>
                <li>
                  <strong>Type:</strong> {characterDetails.type}
                </li>
                <li>
                  <strong>Gender:</strong> {characterDetails.gender}
                </li>
              </ul>
            </ModalDescription>
          </ModalContent>
          <ModalActions>
            <Button color="black" onClick={() => setOpen(false)}>
              Close
            </Button>
          </ModalActions>
        </Modal>
      </Container>
  );
}

export default Home;
