import React, { useState } from "react";
import { NavLink, Navigate } from "react-router-dom";
import { Button, Form, Grid, Segment, Message } from "semantic-ui-react";
import { useAuth } from "../context/AuthContext";
import { api } from "../misc/Api";
import { handleLogError } from "../misc/Helpers";

function Signup() {
  const Auth = useAuth();
  const isLoggedIn = Auth.userIsAuthenticated();

  const [password, setPassword] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [isError, setIsError] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");

  const handleInputChange = (e, { name, value }) => {
    if (name === "password") {
      setPassword(value);
    } else if (name === "name") {
      setName(value);
    } else if (name === "email") {
      setEmail(value);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (!(password && name && email)) {
      setIsError(true);
      setErrorMessage("Please, inform all fields!");
      return;
    }

    const user = { password, name, email };

    try {
      const response = await api.signup(user);
      const { id, name, token } = response.data;
      const authenticatedUser = { id, token, name };

      Auth.userLogin(authenticatedUser);

      setPassword("");
      setName("");
      setEmail("");
      setIsError(false);
      setErrorMessage("");
    } catch (error) {
      handleLogError(error);
      if (error.response && error.response.data) {
        const errorData = error.response.data;
        let errorMessage = "Invalid fields";
        if (errorData.status === 409) {
          errorMessage = errorData.message;
        } else if (errorData.status === 400) {
          errorMessage = errorData.errors[0].defaultMessage;
        }
        setIsError(true);
        setErrorMessage(errorMessage);
      }
    }
  };

  if (isLoggedIn) {
    return <Navigate to="/" />;
  }

  return (
    <Grid textAlign="center">
      <Grid.Column style={{ maxWidth: 450 }}>
        <Form size="large" onSubmit={handleSubmit}>
          <Segment>
            <Form.Input
              fluid
              name="email"
              icon="at"
              iconPosition="left"
              placeholder="Email"
              value={email}
              onChange={handleInputChange}
            />
            <Form.Input
              fluid
              name="password"
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              value={password}
              onChange={handleInputChange}
            />
            <Form.Input
              fluid
              name="name"
              icon="address card"
              iconPosition="left"
              placeholder="Name"
              value={name}
              onChange={handleInputChange}
            />
            <Button color="blue" fluid size="large">
              Signup
            </Button>
          </Segment>
        </Form>
        <Message>
          {`Already have an account? `}
          <NavLink to="/login" color="teal">
            Login
          </NavLink>
        </Message>
        {isError && <Message negative>{errorMessage}</Message>}
      </Grid.Column>
    </Grid>
  );
}

export default Signup;
