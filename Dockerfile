FROM golang:1.22.4-alpine3.20 as backend-builder

WORKDIR /go/src/backend/

COPY /backend/go.mod ./
COPY /backend/go.sum ./

RUN go mod download

COPY /backend/. ./

RUN go build -o backend .

########################################################################################################################

FROM node:20-alpine3.19 as ui-builder

WORKDIR /ui

COPY /ui/public /ui/public
COPY /ui/src /ui/src
COPY /ui/package.json /ui/package.json
COPY /ui/package-lock.json /ui/package-lock.json
RUN npm install
RUN npm run build


########################################################################################################################

FROM alpine:3.20

RUN mkdir -p /app && mkdir -p /logs && \
  apk add --no-cache tzdata
WORKDIR /app

ENV TZ=America/New_York


COPY --from=ui-builder /ui/build /app/build
COPY --from=backend-builder /go/src/backend/backend .

EXPOSE 8080

ENTRYPOINT ["./backend"]
