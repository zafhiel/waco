package handler

import (
	"backend/database"
	"backend/model"

	"github.com/gofiber/fiber/v2"
)

func GetAllFavorites(c *fiber.Ctx) error {
	db := database.DB
	var favorites []model.Favorites
	db.Find(&favorites)
	return c.JSON(fiber.Map{"status": "success", "message": "All favorites", "data": favorites})
}

func ToggledFavorite(c *fiber.Ctx) error {
	db := database.DB
	favorite := new(model.Favorites)
	if err := c.BodyParser(favorite); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't create favorite ", "data": err})
	}

	var exist model.Favorites
	db.First(&exist, "user_id = ? AND character_id = ?", favorite.UserID, favorite.CharacterID)

	if exist.ID != 0 {
		db.Delete(&exist)
		return c.JSON(fiber.Map{"status": "success", "message": "Favorite successfully deleted", "data": nil})
	}

	if err := c.BodyParser(favorite); err != nil {
		return c.Status(500).JSON(fiber.Map{"status": "error", "message": "Couldn't create favorite ", "data": err})
	}

	db.Create(&favorite)
	return c.JSON(fiber.Map{"status": "success", "message": "Created favorite", "data": favorite})
}
