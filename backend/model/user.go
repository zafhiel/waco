package model

import (
	"time"

	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Email    string    `gorm:"uniqueIndex;not null" json:"email"`
	Password string    `gorm:"not null" json:"password"`
	Name     string    `json:"name"`
	Address  string    `json:"address"`
	Birthday time.Time `json:"birthday"`
	City     string    `json:"city"`
}
