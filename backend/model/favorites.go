package model

import "gorm.io/gorm"

// Favorites struct
type Favorites struct {
	gorm.Model
	UserID      int `gorm:"not null" json:"user_id"`
	CharacterID int `gorm:"not null" json:"character_id"`
}
