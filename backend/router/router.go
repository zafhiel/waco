package router

import (
	"backend/handler"
	"backend/middleware"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

// SetupRoutes setup router api
func SetupRoutes(app *fiber.App) {
	app.Static("/", "./build")

	// Middleware
	apiV1 := app.Group("/api/v1", logger.New())

	// Auth
	auth := apiV1.Group("/auth")
	auth.Post("/login", handler.Login)

	// User
	user := apiV1.Group("/user")
	user.Get("/:id", handler.GetUser)
	user.Post("/signup", handler.CreateUser)
	user.Patch("/:id", middleware.Protected(), handler.UpdateUser)
	user.Delete("/:id", middleware.Protected(), handler.DeleteUser)

	// Favorite
	product := apiV1.Group("/favorite")
	product.Get("/", middleware.Protected(), handler.GetAllFavorites)
	product.Post("/", middleware.Protected(), handler.ToggledFavorite)
}
